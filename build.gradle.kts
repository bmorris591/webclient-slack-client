import net.researchgate.release.GitAdapter
import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmCompile

plugins {
    `java-library`
    jacoco
    `maven-publish`
    idea
    id("net.researchgate.release") version "2.8.1"
    kotlin("jvm") version "1.5.0"
    kotlin("plugin.spring") version "1.5.0"
    id("org.jetbrains.dokka") version "1.4.32"
    id("com.gorylenko.gradle-git-properties") version "2.3.1"
    id("com.pasam.gradle.buildinfo") version "0.1.3"
    id("com.github.ben-manes.versions") version "0.38.0"

    id("com.diffplug.spotless") version "5.12.5"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

group = "uk.co.borismorris.slack.webclient"

repositories {
    mavenCentral()
    maven("https://repo.spring.io/milestone")
}

tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs = listOfNotNull(
            "-Xjsr305=strict",
            "-Xjvm-default=enable",
            "-Werror",
            "-progressive",
            "-Xopt-in=kotlin.RequiresOptIn",
            "-Xopt-in=kotlin.ExperimentalUnsignedTypes",
            "-Xopt-in=kotlin.ExperimentalStdlibApi"
        )
    }
}

configurations.all {
    exclude(module = "spring-boot-starter-logging")
    exclude(module = "javax.annotation-api")
    exclude(module = "hibernate-validator")
}

val springBootVersion = "2.5.0-RC1"
val springBootBom = "org.springframework.boot:spring-boot-dependencies:$springBootVersion"

dependencies {
    api(platform(springBootBom))

    compileOnly("be.olsson:slack-appender:1.3.0")

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("org.springframework:spring-web")
    implementation("org.springframework:spring-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

    testImplementation("org.apache.httpcomponents.client5:httpclient5:5.1")
    testImplementation("org.apache.httpcomponents.core5:httpcore5-reactive:5.1.1")
    testImplementation("be.olsson:slack-appender:1.3.0")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.31.0") {
        exclude(group = "org.eclipse.jetty", module = "jetty-alpn-conscrypt-client")
        exclude(group = "org.eclipse.jetty", module = "jetty-alpn-conscrypt-server")
        exclude(group = "org.conscrypt")
        exclude(group = "commons-fileupload")
    }
    testImplementation("org.eclipse.jetty:jetty-alpn-java-server")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core")
    testImplementation("org.awaitility:awaitility:4.1.0")
    testImplementation(kotlin("test-junit5"))

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testRuntimeOnly("org.apache.logging.log4j:log4j-core")
    testRuntimeOnly("org.apache.logging.log4j:log4j-slf4j-impl")
}

spotless {
    kotlin {
        ktlint("0.41.0")
    }
    kotlinGradle {
        ktlint("0.41.0")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()

    testLogging {
        exceptionFormat = TestExceptionFormat.FULL
        showStandardStreams = true
        events("skipped", "failed")
    }
}

jacoco {
    toolVersion = "0.8.7"
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = false
        csv.isEnabled = false
        html.isEnabled = true
    }
}

tasks.jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = "0.8".toBigDecimal()
            }
        }
    }
}

release {
    pushReleaseVersionBranch = "master"
    preCommitText = "[release]"
    val git = propertyMissing("git") as GitAdapter.GitConfig
    git.requireBranch = ""
}

tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("javadoc"))
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
}

val sourcesJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Source code packaged as Jar"
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        create<MavenPublication>("webclient-slack-client") {
            from(components["java"])
            artifact(sourcesJar)
            artifact(dokkaJar)

            pom {
                name.set(project.name)
                description.set("Spring WebClient implementation of the HTTP REST client for the Slack Log4j2 appender")
                url.set("https://gitlab.com/bmorris591/webclient-slack-client/-/wikis/home")

                scm {
                    url.set("https://gitlab.com/bmorris591/webclient-slack-client")
                    connection.set("git@gitlab.com:bmorris591/webclient-slack-client.git")
                    developerConnection.set("git@gitlab.com:bmorris591/webclient-slack-client.git")
                }

                licenses {
                    license {
                        name.set("GNU GPLv3")
                        url.set("https://gitlab.com/bmorris591/webclient-slack-client/blob/master/LICENSE")
                        distribution.set("repo")
                    }
                }

                developers {
                    developer {
                        id.set("bmorris591")
                        name.set("Boris Morris")
                        email.set("bmorris591@gmail.com")
                    }
                }
            }
        }
    }

    repositories {
        maven {
            name = "gitlab-ci"
            url = uri("https://gitlab.com/api/v4/projects/19359150/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("token-header")
            }
        }
    }
}
