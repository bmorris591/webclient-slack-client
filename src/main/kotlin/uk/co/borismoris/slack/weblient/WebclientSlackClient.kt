package uk.co.borismoris.slack.weblient

import be.olsson.slackappender.client.Client
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.scheduler.Schedulers
import reactor.util.retry.Retry
import java.net.URL
import java.time.Duration

class WebclientSlackClient(webclientBuilder: WebClient.Builder) : Client {

    constructor() : this(WebClient.builder())

    private val webClient = webclientBuilder.build()
    private val scheduler = Schedulers.newSingle(WebclientSlackClient::class.qualifiedName!!, true)

    override fun send(webhookUrl: URL?, payload: String?) {
        if (webhookUrl == null) {
            return
        }
        doPost(webhookUrl, payload!!)
            .timeout(Duration.ofSeconds(10))
            .retryWhen(Retry.backoff(5, Duration.ofMillis(100)))
            .checkpoint()
            .then()
            .subscribeOn(scheduler)
            .subscribe(
                {}, // ignore the result
                { it.printStackTrace(System.err) }
            )
    }

    internal fun doPost(webhookUrl: URL, payload: String) = webClient.post()
        .uri(webhookUrl.toURI())
        .contentType(APPLICATION_JSON)
        .bodyValue(payload)
        .retrieve()
        .bodyToMono<String>()
}
