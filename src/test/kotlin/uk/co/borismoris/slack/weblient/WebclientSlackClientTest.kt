package uk.co.borismoris.slack.weblient

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import org.assertj.core.api.Assertions.assertThatCode
import org.awaitility.Awaitility
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.http.client.reactive.HttpComponentsClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import java.net.URL
import java.time.Duration

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class WebclientSlackClientTest {

    @JvmField
    @RegisterExtension
    val wiremock = WireMockExtension.newInstance()
        .options(
            options()
                .dynamicPort()
                .usingFilesUnderClasspath("uk/co/borismoris/slack/weblient/wiremock")
                .notifier(ConsoleNotifier(false))
        )
        .configureStaticDsl(true)
        .build()

    lateinit var client: WebclientSlackClient

    @BeforeAll
    fun setup() {
        client = WebclientSlackClient(WebClient.builder().clientConnector(HttpComponentsClientHttpConnector()))
    }

    @Test
    fun `When message is sent then WebClient API is called with await`() {
        val webhookUrl = URL("${wiremock.runtimeInfo.httpBaseUrl}/slack/webhook?auth=someauthvariable")
        val payload = "Payload Payload Payload"
        client.send(webhookUrl, payload)

        Awaitility.await().atMost(Duration.ofSeconds(10)).pollInterval(Duration.ofSeconds(1)).until {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/slack/webhook")))
            true
        }
    }

    @Test
    fun `When webhook URL is null then no error is thrown`() {
        assertThatCode { client.send(null, null) }.doesNotThrowAnyException()
    }

    @Test
    fun `When message is sent then WebClient API is called`() {
        val webhookUrl = URL("${wiremock.runtimeInfo.httpBaseUrl}/slack/webhook?auth=someauthvariable")
        val payload = "Payload Payload Payload"
        val webhook = client.doPost(webhookUrl, payload)

        StepVerifier.create(webhook)
            .expectNext("Success!")
            .verifyComplete()

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/slack/webhook")))
    }
}
